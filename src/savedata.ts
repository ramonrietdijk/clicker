import store from "./store"
import Inventory from "@/types/Inventory"

function loadGame(): void {
    if (typeof Storage === "undefined") {
        return
    }

    const urlParams = new URLSearchParams(window.location.search)
    const slotIdentifier = urlParams.get("slot") || 0
    const data = {
        money: 0,
        tickCount: 0,
        invert: false,
        bonus: 0,
        items: [],
        inventory: {
            items: [],
            upgrades: [],
            mice: []
        },
        stats: {
            mostMoneyOwned: 0,
            totalClickCount: 0,
            highestClicksPerSecond: 0
        }
    }

    const slot = localStorage.getItem("slot" + slotIdentifier) || btoa(JSON.stringify(data))
    const decryptedData = JSON.parse(atob(slot))

    store.dispatch("setSaveData", decryptedData)
}

function saveGame(data: Inventory): boolean {
    if (typeof Storage === "undefined") {
        return false
    }

    const urlParams = new URLSearchParams(window.location.search)
    const slotIdentifier = urlParams.get("slot") || 0
    const encryptedSaveData = btoa(JSON.stringify(data))

    localStorage.setItem("slot" + slotIdentifier, encryptedSaveData)

    return true
}

export {loadGame, saveGame}