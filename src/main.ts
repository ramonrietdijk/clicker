import {createApp} from "vue"
import App from "./App.vue"
import router from "./router"
import store from "./store"
import "./assets/scss/app.scss"
import TWEEN from "@tweenjs/tween.js"
import {loadGame} from "@/savedata"
import {totalItemMoneyPerSecond} from "@/functions/functions"

createApp(App).use(store).use(router).mount("#app")

// Load the save file.
loadGame()

// Game settings.
const SAVE_INTERVAL = 120000 // 2 minutes
const TICK_INTERVAL = 1000 // 1 second

// Start the timer for auto-saving the game.
setInterval(() => {
    store.dispatch("saveGame")
}, SAVE_INTERVAL)

// Start the timer for ticks.
setInterval(() => {
    store.dispatch("incrementTick")
    store.dispatch("incrementMoney", totalItemMoneyPerSecond())
    store.dispatch("resetClicksPerSecond")
}, TICK_INTERVAL)

function animate(time: number) {
    requestAnimationFrame(animate)
    TWEEN.update(time)
}

requestAnimationFrame(animate)