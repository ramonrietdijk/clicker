import Requirement from "@/types/Requirement"

interface Upgrade {
    id: string
    title: string
    description: string
    price: number
    helper?: string
    priceMultiplier: number
    generationMultiplier: number
    requirements?: Array<Requirement>
    appliesTo: Array<string>
}

export default Upgrade