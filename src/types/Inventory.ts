import InventoryItem from "@/types/InventoryItem"
import InventoryUpgrade from "@/types/InventoryUpgrade"
import InventoryMouse from "@/types/InventoryMouse"

interface Inventory {
    money: number
    tickCount: number
    invert: boolean
    bonus: number
    inventory: {
        items: Array<InventoryItem>
        upgrades: Array<InventoryUpgrade>
        mice: Array<InventoryMouse>
    }
    stats: {
        mostMoneyOwned: number
        totalClickCount: number
        highestClicksPerSecond: number
    }
}

export default Inventory