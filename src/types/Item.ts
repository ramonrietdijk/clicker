interface Item {
    id: string
    title: string
    description: string
    generation: number
    price: number
}

export default Item