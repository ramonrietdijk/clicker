interface InventoryItem {
    id: string
    amount: number
}

export default InventoryItem