interface Mouse {
    id: string
    title: string
    description: string
    percentage: number
    price: number
}

export default Mouse