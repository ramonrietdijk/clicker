import InventoryItem from "@/types/InventoryItem"

interface ValidationRule {
    type: string
    validate: (inventoryItem: InventoryItem, value: number | string) => boolean
}

export default ValidationRule