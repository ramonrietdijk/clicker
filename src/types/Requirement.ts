interface Requirement {
    type: string
    value: number | string
}

export default Requirement