import {createStore} from "vuex"
import {saveGame} from "@/savedata"
import {
    formatCleanNumber,
    getInventoryItemById,
    getInventoryMouseById,
    getInventoryUpgradeById
} from "@/functions/functions"
import InventoryItem from "@/types/InventoryItem"
import InventoryUpgrade from "@/types/InventoryUpgrade"
import InventoryMouse from "@/types/InventoryMouse"

export default createStore({
    state: {
        money: 0,
        tickCount: 0,
        invert: false,
        bonus: 0,
        inventory: {
            items: [] as Array<InventoryItem>,
            upgrades: [] as Array<InventoryUpgrade>,
            mice: [] as Array<InventoryMouse>
        },
        stats: {
            mostMoneyOwned: 0,
            totalClickCount: 0,
            highestClicksPerSecond: 0
        },
        // Session only
        clicksPerSecondCounter: 0,
        clicksPerSecond: 0,
        lastSaved: 0
    },
    getters: {
        money: state => state.money,
        tickCount: state => state.tickCount,
        invert: state => state.invert,
        bonus: state => state.bonus,
        inventory: state => state.inventory,
        stats: state => state.stats,
        clicksPerSecondCounter: state => state.clicksPerSecondCounter,
        clicksPerSecond: state => state.clicksPerSecond,
        lastSaved: state => state.lastSaved
    },
    mutations: {
        setSaveData: function (state, payload) {
            state.money = payload.money || 0
            state.tickCount = payload.tickCount || 0
            state.invert = payload.invert || false
            state.bonus = payload.bonus || 0
            state.inventory.items = payload.inventory.items || []
            state.inventory.upgrades = payload.inventory.upgrades || []
            state.inventory.mice = payload.inventory.mice || []
            state.stats.mostMoneyOwned = payload.stats.mostMoneyOwned || 0
            state.stats.totalClickCount = payload.stats.totalClickCount || 0
            state.stats.highestClicksPerSecond = payload.stats.highestClicksPerSecond || 0
        },
        saveGame: function (state) {
            state.lastSaved = Date.now()
            return saveGame({
                money: state.money || 0,
                tickCount: state.tickCount || 0,
                invert: state.invert || false,
                bonus: state.bonus || 0,
                inventory: {
                    items: state.inventory.items || [],
                    upgrades: state.inventory.upgrades || [],
                    mice: state.inventory.mice || []
                },
                stats: {
                    mostMoneyOwned: state.stats.mostMoneyOwned || 0,
                    totalClickCount: state.stats.totalClickCount || 0,
                    highestClicksPerSecond: state.stats.highestClicksPerSecond || 0
                }
            })
        },
        resetGame: function (state, payload) {
            if (typeof payload === "undefined") {
                payload = {}
            }
            state.money = payload.money || 0
            state.tickCount = payload.tickCount || 0
            state.invert = payload.invert || state.invert // false
            state.bonus = payload.bonus || 0
            state.inventory.items = payload.inventory ? (payload.inventory.items || []) : []
            state.inventory.upgrades = payload.inventory ? (payload.inventory.upgrades || []) : []
            state.inventory.mice = payload.inventory ? (payload.inventory.mice || []) : []
            state.stats.mostMoneyOwned = payload.stats ? (payload.stats.mostMoneyOwned || 0) : 0
            state.stats.totalClickCount = payload.stats ? (payload.stats.totalClickCount || 0) : 0
            state.stats.highestClicksPerSecond = payload.stats ? (payload.stats.highestClicksPerSecond || 0) : 0
        },
        incrementMoney: function (state, payload) {
            state.money += payload
            document.title = `$${formatCleanNumber(state.money)} - Clicker`
            if (state.money > state.stats.mostMoneyOwned) {
                state.stats.mostMoneyOwned = state.money
            }
        },
        incrementTick: function (state) {
            state.tickCount++
        },
        incrementClick: function (state) {
            state.stats.totalClickCount++
            state.clicksPerSecondCounter++
        },
        resetClicksPerSecond: function (state) {
            state.clicksPerSecond = state.clicksPerSecondCounter
            if (state.clicksPerSecond > state.stats.highestClicksPerSecond) {
                state.stats.highestClicksPerSecond = state.clicksPerSecond
            }
            state.clicksPerSecondCounter = 0
        },
        toggleInvert: function (state) {
            state.invert = !state.invert
        },
        purchaseItem: function (state, payload) {
            const foundItem = getInventoryItemById(payload.item.id)

            if (typeof foundItem === "undefined") {
                state.inventory.items.push({id: payload.item.id, amount: 1})
                state.money -= payload.price
            } else {
                foundItem.amount++
                state.money -= payload.price
            }
        },
        purchaseUpgrade: function (state, payload) {
            if (typeof getInventoryUpgradeById(payload.upgrade.id) === "undefined") {
                state.inventory.upgrades.push({id: payload.upgrade.id})
                state.money -= payload.price
            }
        },
        purchaseMouse: function (state, payload) {
            if (typeof getInventoryMouseById(payload.mouse.id) === "undefined") {
                state.inventory.mice.push({id: payload.mouse.id})
                state.money -= payload.price
            }
        }
    },
    actions: {
        setSaveData: function (context, payload) {
            context.commit("setSaveData", payload)
        },
        saveGame: function (context) {
            context.commit("saveGame")
        },
        resetGame: function (context, payload) {
            context.commit("resetGame", payload)
        },
        incrementMoney: function (context, payload) {
            context.commit("incrementMoney", payload)
        },
        incrementTick: function (context) {
            context.commit("incrementTick")
        },
        incrementClick: function (context) {
            context.commit("incrementClick")
        },
        resetClicksPerSecond: function (context) {
            context.commit("resetClicksPerSecond")
        },
        toggleInvert: function (context) {
            context.commit("toggleInvert")
        },
        purchaseItem: function (context, payload) {
            context.commit("purchaseItem", payload)
        },
        purchaseUpgrade: function (context, payload) {
            context.commit("purchaseUpgrade", payload)
        },
        purchaseMouse: function (context, payload) {
            context.commit("purchaseMouse", payload)
        }
    }
})
