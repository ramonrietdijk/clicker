import Mouse from "@/types/Mouse"

const Mice: Array<Mouse> = [
    {
        id: "ccffa468-cef4-4083-8629-4c0696c11544",
        title: "5 Is Greater Than 1",
        description: "Every click becomes 5% of the total money per second.",
        percentage: 5,
        price: 2000
    },
    {
        id: "9b60e18e-4c15-4689-8a78-97f5b4496ee5",
        title: "The Giant Mouse Upgrade",
        description: "I'm sure this will help in your click-adventure!",
        percentage: 15,
        price: 60000
    },
    {
        id: "51abce63-967b-4397-a6a1-1c0d6d53aa67",
        title: "The Broken Mouse",
        description: "Hmm, I wonder how this could have happened...",
        percentage: 25,
        price: 1000000
    },
]

export default Mice