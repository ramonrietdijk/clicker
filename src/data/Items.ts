import Item from "@/types/Item"

const Items: Array<Item> = [
    {
        id: "df08e857-8c13-471b-9e04-332dcd21f5b0",
        title: "An Old Keyboard",
        description: "What is better than an old keyboard? Well... a new one, but whatever!",
        generation: 0.25,
        price: 25
    },
    {
        id: "90e58105-a821-4f32-af68-fb5c0faa7c3c",
        title: "Office Chair",
        description: "Be sure to sit comfy when you're busy working, I am right?",
        generation: 2.5,
        price: 300
    },
    {
        id: "07300fae-e286-4eb5-a040-b8b0e4afc2ca",
        title: "Office",
        description: "Computers... a lot of computers. That is all I have to say about this one.",
        generation: 18,
        price: 2700
    },
    {
        id: "1d0544db-015c-40f6-989f-d710a9416e3a",
        title: "Beginners Guide: Crypto",
        description: "Hmm, some information that I can certainly use...",
        generation: 45,
        price: 7500
    },
    {
        id: "395b2fa3-6e0b-4895-a61a-149be0d109b4",
        title: "Crypto Miner",
        description: "Here the real fun starts, I hope you're prepared for this one!",
        generation: 105,
        price: 18250
    },
    {
        id: "b0ca7198-51f1-4b14-8d74-e90cde914f2f",
        title: "Crypto Miner 2.0",
        description: "The same miner, but with some handy advancements!",
        generation: 225,
        price: 45000
    },
    {
        id: "c048fe19-6450-47aa-ba2e-45dcafa90f0b",
        title: "Some Strong Coffee",
        description: "In order to sustain maximal productivity, strong coffee is required.",
        generation: 1500,
        price: 350000
    },
    {
        id: "9ab1de3e-1a68-46f5-b7b3-53d861cdda13",
        title: "Self-awareness AI",
        description: "The new time perk has only just begun... be careful!",
        generation: 20000,
        price: 5000000
    },
    {
        id: "3549600c-55b6-485d-aed9-5be684bab747",
        title: "Gaint Datacenter",
        description: "The information which has been gathered has to be stored somewhere!",
        generation: 315000,
        price: 95000000
    },
    {
        id: "a5ac6986-a5f6-48d9-97c4-c29bf0c3a57e",
        title: "Quantum Computing Evolved",
        description: "More information = more profit!",
        generation: 2600000,
        price: 1200000000
    }
]

export default Items