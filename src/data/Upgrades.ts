import Upgrade from "@/types/Upgrade"

const Upgrades: Array<Upgrade> = [
    {
        id: "a9ecc610-5ffa-4704-a477-e80df5eb16dc",
        title: "New Keyboard Caps",
        description: "I could surely use some new keyboard caps.",
        price: 400,
        priceMultiplier: 1,
        generationMultiplier: 2,
        appliesTo: [
            "df08e857-8c13-471b-9e04-332dcd21f5b0"
        ]
    },
    {
        id: "f9b0be27-c429-4b29-8527-c766fcc17307",
        title: "Small Discount",
        description: "Why do I have so many keyboards?",
        helper: "You need at least 100 keyboards to purchase this upgrade.",
        price: 10000,
        requirements: [
            {
                type: "MinimalAmount",
                value: 100
            }
        ],
        priceMultiplier: 0.85,
        generationMultiplier: 2,
        appliesTo: [
            "df08e857-8c13-471b-9e04-332dcd21f5b0"
        ]
    },
    {
        id: "4e0bc534-790a-44be-be10-474daa3a5f85",
        title: "Rotation Upgrade",
        description: "Increased the rotation speed of the office chair by 100%.",
        price: 4800,
        priceMultiplier: 1,
        generationMultiplier: 2,
        appliesTo: [
            "90e58105-a821-4f32-af68-fb5c0faa7c3c"
        ]
    },
    {
        id: "af2c127b-d71b-48f1-80a3-26e36089b8e4",
        title: "Space Efficiency",
        description: "Extend the amount of computers in the office by taping them on the ceiling.",
        price: 43200,
        priceMultiplier: 1,
        generationMultiplier: 2,
        appliesTo: [
            "07300fae-e286-4eb5-a040-b8b0e4afc2ca"
        ]
    },
    {
        id: "c7feecad-d5a2-4d17-9484-ae9f09165493",
        title: "Comic Sans Is Awesome",
        description: "Replace the guide's font to Comic Sans.",
        price: 120000,
        priceMultiplier: 1,
        generationMultiplier: 2,
        appliesTo: [
            "1d0544db-015c-40f6-989f-d710a9416e3a"
        ]
    },
    {
        id: "3f67a4e9-e96d-4d3a-b4fd-78d08b697f48",
        title: "Overclocking+",
        description: "Increase the generation with 100% by overclocking the system.",
        price: 292000,
        priceMultiplier: 1,
        generationMultiplier: 2,
        appliesTo: [
            "395b2fa3-6e0b-4895-a61a-149be0d109b4"
        ]
    },
    {
        id: "a6e396c3-0124-48aa-b037-fb61feaaa168",
        title: "Overclocking+2",
        description: "Everything has to be overclocked nowadays...",
        price: 720000,
        priceMultiplier: 1,
        generationMultiplier: 2,
        appliesTo: [
            "b0ca7198-51f1-4b14-8d74-e90cde914f2f"
        ]
    },
    {
        id: "bf5c46e7-fba4-4e87-9e09-13b81eb1e28a",
        title: "Extra Caffeine",
        description: "For the people who think strong coffee itself wasn't good enough.",
        price: 5600000,
        priceMultiplier: 1,
        generationMultiplier: 2,
        appliesTo: [
            "c048fe19-6450-47aa-ba2e-45dcafa90f0b"
        ]
    },
    {
        id: "8cdad9c5-845b-435a-b7ca-81af1169bf19",
        title: "What Is Life?",
        description: "Finally get answers on all the questions you ever had!",
        price: 80000000,
        priceMultiplier: 1,
        generationMultiplier: 2,
        appliesTo: [
            "9ab1de3e-1a68-46f5-b7b3-53d861cdda13"
        ]
    },
    {
        id: "edbcacb7-4fe4-429b-9ed6-8bf7753de554",
        title: "Giant Squared",
        description: "Increase the size of the datacenter even more to maximize capacity.",
        price: 1520000000,
        priceMultiplier: 1,
        generationMultiplier: 2,
        appliesTo: [
            "3549600c-55b6-485d-aed9-5be684bab747"
        ]
    },
    {
        id: "58ba5618-45ec-45f9-8997-1b725753e1e8",
        title: "Is This The Last Upgrade?",
        description: "Yes! You have made it all the way to the end of the game!",
        price: 19200000000,
        priceMultiplier: 1,
        generationMultiplier: 2,
        appliesTo: [
            "a5ac6986-a5f6-48d9-97c4-c29bf0c3a57e"
        ]
    }
]

export default Upgrades