import Items from "@/data/Items"
import Item from "@/types/Item"
import Upgrade from "@/types/Upgrade"
import InventoryItem from "@/types/InventoryItem"
import store from "@/store"
import InventoryUpgrade from "@/types/InventoryUpgrade"
import Upgrades from "@/data/Upgrades"
import Mice from "@/data/Mice"
import Mouse from "@/types/Mouse"
import InventoryMouse from "@/types/InventoryMouse"

const units = ['k', 'm', 'g', 't', 'p', 'e', 'z', 'y']
const priceIncreasePercentage = 0.075 // 7.5%

function formatCleanNumber(number: number, digits = 2): string {
    let decimal = 0

    for (let i = units.length - 1; i >= 0; i--) {
        decimal = Math.pow(1000, i + 1)

        if (Math.abs(number) >= decimal) {
            return +(number / decimal).toFixed(digits) + units[i]
        }
    }

    return formatRawNumber(number, digits)
}

function formatRawNumber(number: number, digits = 2): string {
    return number.toFixed(digits).replace(/\B(?=(\d{3})+(?!\d))/g, '.')
}

function getItemById(id: string): Item | undefined {
    return Items.find((item: Item) => item.id === id)
}

function getUpgradeById(id: string): Upgrade | undefined {
    return Upgrades.find((upgrade: Upgrade) => upgrade.id === id)
}

function getMouseById(id: string): Mouse | undefined {
    return Mice.find((mouse: Mouse) => mouse.id === id)
}

function getInventoryItemById(id: string): InventoryItem | undefined {
    return store.getters.inventory.items.find((inventoryItem: InventoryItem) => inventoryItem.id === id)
}

function getInventoryUpgradeById(id: string): InventoryUpgrade | undefined {
    return store.getters.inventory.upgrades.find((inventoryUpgrade: InventoryUpgrade) => inventoryUpgrade.id === id)
}

function getInventoryMouseById(id: string): Mouse | undefined {
    return store.getters.inventory.mice.find((inventoryMouse: InventoryMouse) => inventoryMouse.id === id)
}

function getPurchasedUpgrades(item?: Item): Array<Upgrade> {
    const upgrades = [] as Array<Upgrade>

    store.getters.inventory.upgrades.forEach((inventoryUpgrade: InventoryUpgrade) => {
        const upgrade = getUpgradeById(inventoryUpgrade.id)

        if (typeof upgrade !== "undefined") {
            upgrades.push(upgrade)
        }
    })

    if (typeof item !== "undefined") {
        return upgrades.filter((upgrade: Upgrade) => upgrade.appliesTo.indexOf(item.id) !== -1)
    }

    return upgrades
}

function amountOfItem(item: Item): number {
    return getInventoryItemById(item.id)?.amount || 0
}

function calculatedPrice(item: Item): number {
    const upgrades = getPurchasedUpgrades(item)
    const amount = amountOfItem(item)

    let price = Math.round(item.price * (1 + (priceIncreasePercentage * amount)))

    upgrades.forEach((upgrade: Upgrade) => price *= upgrade.priceMultiplier)

    return price
}

function itemGeneration(item: Item): number {
    let totalMultiplier = 1

    const upgrades = getPurchasedUpgrades(item)

    upgrades.forEach((upgrade: Upgrade) => totalMultiplier *= upgrade.generationMultiplier)

    return item.generation * totalMultiplier * (1 + (store.getters.bonus / 100))
}

function itemMoneyPerSecond(item: Item): number {
    return itemGeneration(item) * (getInventoryItemById(item.id)?.amount || 0)
}

function itemMoneyPerSecondPercentage(item: Item): number {
    const totalMoneyPerSecond = totalItemMoneyPerSecond()

    return totalMoneyPerSecond === 0 ? 0 : Math.round(itemMoneyPerSecond(item) / totalMoneyPerSecond * 100)
}

function totalItemMoneyPerSecond(): number {
    let moneyPerSecond = 0

    store.getters.inventory.items.forEach((inventoryItem: InventoryItem) => {
        const item = getItemById(inventoryItem.id)

        if (typeof item !== "undefined") {
            moneyPerSecond += itemMoneyPerSecond(item)
        }
    })

    return moneyPerSecond
}

export {
    formatCleanNumber,
    formatRawNumber,
    getItemById,
    getUpgradeById,
    getMouseById,
    getInventoryItemById,
    getInventoryUpgradeById,
    getInventoryMouseById,
    getPurchasedUpgrades,
    amountOfItem,
    calculatedPrice,
    itemGeneration,
    itemMoneyPerSecond,
    itemMoneyPerSecondPercentage,
    totalItemMoneyPerSecond
}