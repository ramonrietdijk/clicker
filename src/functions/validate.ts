import ValidationRule from "@/types/ValidationRule"
import Requirement from "@/types/Requirement"
import InventoryItem from "@/types/InventoryItem"

const rules: Array<ValidationRule> = []

rules.push({
    type: "MinimalAmount",
    validate: (inventoryItem: InventoryItem, value: number | string): boolean => {
        return inventoryItem.amount >= value
    }
})

function validate(inventoryItem: InventoryItem, requirement: Requirement): boolean {
    const rule = rules.find(rule => rule.type === requirement.type)

    if (typeof rule === "undefined")
        return false

    return rule.validate(inventoryItem, requirement.value)
}

export default validate