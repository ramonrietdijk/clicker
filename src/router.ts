import {createRouter, createWebHistory, RouteRecordRaw} from "vue-router"
import Clicker from "@/views/Clicker.vue"

const routes: Array<RouteRecordRaw> = [
    {
        path: "/",
        name: "Clicker",
        component: Clicker
    }
]

const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router
