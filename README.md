# Clicker

Clicker is a JavaScript game written in Vue.js 3 and TypeScript using the composition API. The goal is to make as much
money as possible in order to purchase every upgrade in the store.

## Installation

In order to use the application you'll have to follow the steps below.

### Install node modules

```
npm install
```

## Usage

There are a few npm scripts available in order to use the application.

### Running a development server

The following command enables compilation and hot-reloads for development.

```
npm run serve
```

### Production

The following command compiles and minifies the code for production.

```
npm run build
```

### Linter

The following command searches for programming errors, stylistic errors, etc.

```
npm run lint
```

## License

This project is released under the [MIT](https://gitlab.com/ramonrietdijk/clicker/-/blob/master/LICENSE) license.
